#!/usr/bin/env bash

# Check if all env variables are there
[ -z $ACTION ] && echo "ACTION not defined" && exit -1;

function backup {
[ -z $BITBUCKET_HOME ] && echo "BITBUCKET_HOME not defined" && exit -1;
[ -z $BITBUCKET_USER ] && echo "BITBUCKET_USER not defined" && exit -1;
[ -z $BITBUCKET_PASSWORD ] && echo "BITBUCKET_PASSWORD not defined" && exit -1;
[ -z $BITBUCKET_BASEURL ] && echo "BITBUCKET_BASEURL not defined" && exit -1;
[ -z $BACKUP_HOME ] && echo "BACKUP_HOME not defined" && exit -1;


cd $WORKDIR

java -Dbitbucket.home="${BITBUCKET_HOME}" -Dbitbucket.user="${BITBUCKET_USER}" \
  -Dbitbucket.password="${BITBUCKET_PASSWORD}" -Dbitbucket.baseUrl="${BITBUCKET_BASEURL}" \
  -Dbackup.home="${BACKUP_HOME}" -jar bitbucket-backup-client.jar

}


function restore {
[ -z $ORACLE_HOSTNAME ] && echo "ORACLE_HOSTNAME not defined" && exit -1;
[ -z $ORACLE_PORT ] && echo "ORACLE_PORT not defined" && exit -1;
[ -z $ORACLE_SERVICE ] && echo "ORACLE_SERVICE not defined" && exit -1;
[ -z $ORACLE_USER ] && echo "ORACLE_USER not defined" && exit -1;
[ -z $ORACLE_PASSWORD ] && echo "ORACLE_PASSWORD not defined" && exit -1;
[ -z $RESTORE_HOME ] && echo "RESTORE_HOME not defined" && exit -1;
[ -z $BACKUP_FILE ] && echo "BACKUP_FILE not defined" && exit -1;


cd $WORKDIR

java -Djdbc.override=true -Djdbc.driver=oracle.jdbc.driver.OracleDriver \
  -Djdbc.url=jdbc:oracle:thin:@//"${ORACLE_HOSTNAME}":"${ORACLE_PORT}"/"${ORACLE_SERVICE}" \
  -Djdbc.user="${ORACLE_USER}" -Djdbc.password="${ORACLE_PASSWORD}" -Dbitbucket.home="${RESTORE_HOME}" \
  -jar bitbucket-restore-client.jar ${BACKUP_FILE}

}


# What to execute
if [ "$ACTION" = "backup" ]; then
	backup
elif  [ "$ACTION" = "restore" ]; then
	restore
fi


